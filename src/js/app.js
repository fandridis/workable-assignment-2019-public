import {
	getGenresList,
	getCurrentMovies,
	getUpcomingMovies,
	searchMovieByTitle,
	getVideo,
	getReviews,
	getSimilar,
} from './api';

/**
 * APPLICATION STATE
 * Anything needs tracking lives here
 */
const state = {
	inDisplay: 'current', 				// Possible values: ['current', 'upcoming', 'filtered']
	reviewsLimit: 2,							// How many reviews should be shown in page
	similarMoviesLimit: 3,				// How many similar movies should be shown in the page
	currentlyExpandedCard: null,
	searchValue: null,
	isLoading: false,

	genres: [],

	current: {
		movies: [],
		totalMovies: 0,
		page: 0,
		totalPages: 0,
	},
	upcoming: {
		movies: [],
		totalPages: 0,
		currentPage: 0,
		totalMovies: 0,
	},
	filtered: {
		movies: [],
		totalPages: 0,
		currentPage: 0,
		totalMovies: 0,
	}
}

/**
 * HTML ELEMENTS TARGETED BY JS
 */
const htmlEl = document.documentElement;
const bodyEl = document.body;
const appEl = document.querySelector('#app');

const CurrentCategoryEl = document.querySelector('#movies__current');
const UpcomingCategoryEl = document.querySelector('#movies__upcoming');
const searchInputEl = document.querySelector('#search-movie');
const movieTemplateEl = document.querySelector('#movie-template');
const moviesList = document.querySelector('#movies__grid');

const reviewTemplateEl = document.querySelector('#review-template');
const similarMovilesTemplateEl = document.querySelector('#similar-template');
const similarMoviesListEl = document.querySelector('.overlay__similar');
const movieDetailsOverlayEl = document.querySelector('#movie-details-overlay');
const reviewsListEl = document.querySelector('.overlay__reviews');
const similarListEl = document.querySelector('.overlay__similar');
const noTrailerEl = document.querySelector('.no-trailer');
const overlayEl = document.querySelector('.overlay');

const movieReleaseDateEl = overlayEl.querySelector('.overlay__release-date');
const movieTrailerEl = overlayEl.querySelector('.overlay__video');
const movieTitleEl = overlayEl.querySelector('.overlay__title');
const movieOverviewEl = overlayEl.querySelector('.overlay__overview')

/**
 * EVENT LISTENERS
 */
document.addEventListener('scroll', _.throttle(() => onScrollListener(), 100));
document.querySelector('#search-movie').addEventListener('input', _.debounce((e) => onSearchMovie(e), 700));
document.querySelector('#movies__current').addEventListener('click', () => onCategorySelect('current'));
document.querySelector('#movies__upcoming').addEventListener('click', () => onCategorySelect('upcoming'));
document.querySelector('#button-back').addEventListener('click', onCloseDetails);

/**
 * INITIALIZATION OF THE APP
 * Fetch the genres list (as the movies only keep the genre IDs)
 * Fetch the 1st of movies now in theaters and render them in the page
 */
(document.onload = () => {
	const promises = [
		getGenresList(),
		getCurrentMovies(1),
	];

	Promise.all(promises)
		.then (res => {
			const genresData = res[0];
			const moviesData = res[1];

			state.genres = genresData.genres;
			state.current.movies = moviesData.results;
			state.current.totalMovies = moviesData.total_results;
			state.current.page = moviesData.page;
			state.current.totalPages = moviesData.total_pages;
			
			appendMoviesToDOM(moviesData.results);
		})
		.catch(err => console.log('Error: ', err))
})();


/**
 * FUNCTIONS THAT HANDLE USER INTERACTION WITH THE DOM
 */
function onCategorySelect(category) {
	if (category === state.inDisplay) { return; }

	handleCategoryChange(category);
	render();
}

function onSearchMovie(e) {
	const { value } = e.target;
	state.searchValue = value;
	// If user cleared the input, switch to movies in theaters
	if (value.length === 0) {
		handleCategoryChange('current');
		return render();
	}
	// Otherwise, switch to filtered movies and render the results
	handleCategoryChange('filtered');
	render();
}

function onOpenDetails(movie) {
	const selector = `[data-id="${movie.id}"]`;
	const cardEl = document.querySelector(selector);

	expandCard(cardEl, movie);
}

function onCloseDetails() {
	state.currentlyExpandedCard.classList.remove('movie__card--expanded');
	htmlEl.classList.remove('no-scroll');
	bodyEl.classList.remove('no-scroll');
	appEl.classList.remove('no-scroll');

	movieDetailsOverlayEl.scroll(0,0);
	movieDetailsOverlayEl.style.display = 'none';

	clearOverlayContent();
}

function handleCategoryChange(category) {
	state.inDisplay = category;

	if (category === 'current') {
		CurrentCategoryEl.classList.add('isActive');
		UpcomingCategoryEl.classList.remove('isActive');
		searchInputEl.value = '';
	} else if (category === 'upcoming') {
		CurrentCategoryEl.classList.remove('isActive');
		UpcomingCategoryEl.classList.add('isActive');
		searchInputEl.value = '';
	} else {
		CurrentCategoryEl.classList.remove('isActive');
		UpcomingCategoryEl.classList.remove('isActive');
	}
}

/**
 * SCROLL LISTENER CALLBACK
 * Will attempt to load more movies if the user is 100px away 
 * from reaching the last movie card of the list.
 */
function onScrollListener() {
	var lastCard = document.querySelector('#movies__grid > article:last-child');

	if (!lastCard) { return; }

	var lastCardOffset = lastCard.offsetTop + lastCard.clientHeight;
	var pageOffset = window.pageYOffset + window.innerHeight;

	if (pageOffset > lastCardOffset - 100) {
		loadMoreMovies();
	}
};

function loadMoreMovies() {
	const { inDisplay, searchValue } = state;

	if (state[inDisplay].page < state[inDisplay].totalPages && !state.isLoading) {
		let apiCall;

		toggleLoading();

		if (state.inDisplay === 'current') {
			apiCall = getCurrentMovies;
		} else if (state.inDisplay === 'upcoming') {
			apiCall = getUpcomingMovies;
		} else if (state.inDisplay === 'filtered') {
			apiCall = searchMovieByTitle;
		}

		apiCall(state[inDisplay].page + 1, searchValue)
			.then(moviesData => {
				state[inDisplay].movies = [...state[inDisplay].movies, ...moviesData.results];
				state[inDisplay].page = moviesData.page;

				toggleLoading();
				appendMoviesToDOM(moviesData.results);
			})
			.catch(err => console.log('Error: ', err))
	}
}

function appendMoviesToDOM(movies) {
	const basePosterUrl = 'https://image.tmdb.org/t/p/w500';

	for (let movie of movies) {
		const clone = document.importNode(movieTemplateEl.content, true);
		// Update the details of the template with the current movie
		clone.querySelector('.movie__title').textContent = movie.original_title;
		clone.querySelector('.movie__genres').textContent = generateGenresList(movie);
		clone.querySelector('.movie__score').textContent = movie.vote_average;
		// Add the movie's poster or a placeholder image if the movie doesn't have a poster
		if (movie.poster_path) {
			clone.querySelector('.movie__card img').setAttribute('src', `${basePosterUrl}/${movie.poster_path}`);
		} else {
			clone.querySelector('.movie__card img').setAttribute('src', 'https://gefa-public-assets.s3.eu-central-1.amazonaws.com/images/cinema-curtains-small.png');
		}
		// Add the id of the movie to the movie-element so it can be targeted in the future
		clone.querySelector('.movie__card').dataset.id = movie.id;
		// Add a click event listener, used for expanding the movie card
		clone.querySelector('.movie__see-more').addEventListener('click', () => onOpenDetails(movie));
		// Finally, append the new movie-card to the DOM
		moviesList.appendChild(clone)
	}
}

function toggleLoading() {
	state.isLoading = !state.isLoading;

	if (state.isLoading) {
		// Create the loader element and append it to the DOM
		const loaderWrapper = document.createElement('div');
		const loader = document.createElement('div');

		loaderWrapper.classList.add('loader-wrapper');
		loader.classList.add('loader');

		loaderWrapper.append(loader);
	 	moviesList.append(loaderWrapper);
	} else {
		// Find the loader and remove it from the DOM
		const loaderWrapper = document.querySelector('.loader-wrapper');
		moviesList.removeChild(loaderWrapper)
	}
}

function generateGenresList(movie) {
	return state.genres
		.filter(genre => movie.genre_ids.includes(genre.id))
		.map(genre => genre.name)
		.join(', ')
}

function clearMoviesList() {
	while (moviesList.firstChild) {
		moviesList.removeChild(moviesList.firstChild);
	}
}

function getMoviesAndRender() {
	const { inDisplay, searchValue } = state;
	let apiCall;

	if (state.inDisplay === 'current') {
		apiCall = getCurrentMovies
	} else if (state.inDisplay === 'upcoming') {
		apiCall = getUpcomingMovies
	} else if (state.inDisplay === 'filtered') {
		apiCall = searchMovieByTitle
	}

	apiCall(1, searchValue)
		.then(moviesData => {
			state[inDisplay].movies = moviesData.results;
			state[inDisplay].totalMovies = moviesData.total_results;
			state[inDisplay].page = moviesData.page;
			state[inDisplay].totalPages = moviesData.total_pages;

			appendMoviesToDOM(moviesData.results);
		})
		.catch(err => console.log('Error: ', err))
}

function render() {
	const { inDisplay } = state;
	// First clear the movies list
	clearMoviesList();
	// If we already have the movies of this category, just render them
	if (inDisplay !== 'filtered' && state[inDisplay].movies.length > 0) {
		return appendMoviesToDOM(state[inDisplay].movies);
	}
	// Otherwise, fetch them from the API and render them
	getMoviesAndRender();
}


/**
 * FUNCTIONS FOR EXPANDING/CLOSING A CARD
 */
function expandCard(card, movie) {
	htmlEl.classList.add('no-scroll');
	bodyEl.classList.add('no-scroll');
	appEl.classList.add('no-scroll');
	card.classList.add('movie__card--expanded');

	state.currentlyExpandedCard = card;

	prepareOverlayContent(movie);

	setTimeout(() => {
	  movieDetailsOverlayEl.style.display = 'block';
	}, 300);
}

function prepareOverlayContent(movie) {
	movieTitleEl.textContent = movie.original_title;
	movieReleaseDateEl.textContent += movie.release_date;
	movieOverviewEl.textContent = movie.overview;

		const promises = [
			getVideo(movie.id),
			getReviews(movie.id),
			getSimilar(movie.id),
		];
	
		Promise.all(promises)
			.then (res => {
				const videoData = res[0];
				const reviewsData = res[1];
				const similarData = res[2];

				generateTrailer(videoData.results);
				generateReviews(reviewsData.results);
				generateSimilarMovies(similarData.results);
			})
			.catch(err => console.log('Error: ', err))
}

function clearOverlayContent() {
	movieReleaseDateEl.textContent = 'Release Date: ';
	movieTrailerEl.setAttribute('src', 'about:blank');
	movieTrailerEl.style.display = 'initial';

	while (reviewsListEl.firstChild) {
		reviewsListEl.removeChild(reviewsListEl.firstChild);
	}

	while (similarListEl.firstChild) {
		similarListEl.removeChild(similarListEl.firstChild);
	}

	if (noTrailerEl.firstChild) {
		noTrailerEl.removeChild(noTrailerEl.firstChild);
	}
}

function generateTrailer(videos) {
	// If the movie has a trailer to show, generate the url and inject it in the iframe
	// Otherwise, hide the iframe and show a "no trailer" message.
	if (videos.length > 0) {
		const videoUrl = `https://www.youtube.com/embed/${videos[0].key}`;
		movieTrailerEl.setAttribute('src', videoUrl);
	} else {
		const notFoundEl = createNotFoundEl('trailer');
		movieTrailerEl.style.display = 'none';
		noTrailerEl.append(notFoundEl);
	}
}

function generateReviews(reviews) {
	let counter = 1;

	if (!reviews.length) {
		const notFoundEl = createNotFoundEl('reviews');
		return reviewsListEl.append(notFoundEl);
	}

	for (let review of reviews) {
		const clone = document.importNode(reviewTemplateEl.content, true);
		// Update the details of the template with the current movie
		clone.querySelector('.review__author').textContent = review.author;
		clone.querySelector('.review__content').textContent = review.content;
		// Finally, append the new review-card to the DOM
		reviewsListEl.appendChild(clone)
		// Stop appending more reviews if we have reached the limit (configured from state)
	  if (++counter > state.reviewsLimit) {
			break;
		}
	}
}

function generateSimilarMovies(movies) {
	const basePosterUrl = 'https://image.tmdb.org/t/p/w500';
	let counter = 1;

	if (!movies.length) {
		const notFoundEl = createNotFoundEl('similar movies');
		return similarMoviesListEl.append(notFoundEl);
	}

	for (let movie of movies) {
		const clone = document.importNode(similarMovilesTemplateEl.content, true);
		// Update the details of the template with the current movie
		clone.querySelector('.similar__title').textContent = movie.original_title;
		
		if (movie.poster_path) {
			clone.querySelector('.similar__poster').setAttribute('src', `${basePosterUrl}/${movie.poster_path}`);
		} else {
			clone.querySelector('.similat__poster').setAttribute('src', 'https://gefa-public-assets.s3.eu-central-1.amazonaws.com/images/cinema-curtains-small.png');
		}
		// Finally, append the new review-card to the DOM
		similarMoviesListEl.appendChild(clone)
		// Stop appending more similar movies if we have reached the limit (configured from state)
	  if (++counter > state.similarMoviesLimit) {
			break;
		}
	}
}

function createNotFoundEl(text) {
	const notFoundWrapper = document.createElement('div');
	const notFound = document.createElement('p');

	notFoundWrapper.classList.add('not-found-wrapper');
	notFound.classList.add('not-found');
	notFound.textContent=`No ${text} found`;
	notFoundWrapper.append(notFound);
	
	return notFoundWrapper;
}