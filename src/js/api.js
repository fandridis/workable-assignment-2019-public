// Appends the API key to the requesdt
// In real situations, it should not be available to public,
// but added as an environmental variable.
function getUrl(url) {
	const urlWithKey = url + '?api_key=VALID_API_KEY';

	return urlWithKey;
}

// Return the genres related to a movie
export function getGenresList() {
	let url = getUrl('https://api.themoviedb.org/3/genre/movie/list')

	return fetch(url)
  	.then(res => res.json())
}

// Return movies with similar title
export function searchMovieByTitle(page, searchValue) {
	let url = getUrl('https://api.themoviedb.org/3/search/movie');
	url += encodeURI(`&query=${searchValue}`);

	if (page) {
		url += `&page=${page}`;
	}

	return fetch(url)
  	.then(res => res.json())
}

// Return movies currently playing in theaters
export function getCurrentMovies(page) {
	let url = getUrl('https://api.themoviedb.org/3/movie/now_playing');

	if (page) {
		url += `&page=${page}`;
	}

	return fetch(url)
  	.then(res => res.json())
}

// Return movies that will be released soon
export function getUpcomingMovies(page) {
	let url = getUrl('https://api.themoviedb.org/3/movie/upcoming');

	if (page) {
		url += `&page=${page}`;
	}

	return fetch(url)
  	.then(res => res.json())
}

// Return any available videos/trailers of a movie
export function getVideo(movieId) {
	let url = getUrl(`https://api.themoviedb.org/3/movie/${movieId}/videos`);

	return fetch(url)
  	.then(res => res.json())
}

// Return any available reviews made for a movie
export function getReviews(movieId) {
	let url = getUrl(`https://api.themoviedb.org/3/movie/${movieId}/reviews`);

	return fetch(url)
  	.then(res => res.json())
}

// Return movies similar to a movie
export function getSimilar(movieId) {
	let url = getUrl(`https://api.themoviedb.org/3/movie/${movieId}/similar`);

	return fetch(url)
  	.then(res => res.json())
}
