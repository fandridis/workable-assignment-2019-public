# A simple application for viewing the current and upcoming movies in theaters.
*Made with HTML, CSS and Vanilla Javascript.*

---------------------------------------------------
### Live Demo: https://movierama.netlify.com/
---------------------------------------------------


## Installation
*Follow the steps below to work on this repo locally:*

1. Clone the repo and install dependancies: **```npm install```**
   
2. Add a valid TheMovieDB API key at api.js

3. Start the local server and listen on changes: **```npm run dev```**

4. Work on **src** folder and watch the magic happen at **localhost:1234**


## Production
*To build a production-ready state of the application:*
**```npm run build```**

Which creates a folder "build" with:

- minified/prefixed CSS
- ES5 translated Javascript

---

# DOCUMENTATION

## FIRST WORDS
At first, I was bummed that I could not use **React** (or maybe **Vue**) for this project,
as it is the perfect candidate for a SPA made with one of those frameworks.

Having that said, I decided to write code using the latest CSS and JS and use **PARCEL**
as a bundler to transform it to work with most browsers. 

## STYLING
As there are not many different pieces on the application, I stayed away from libraries like bootstrap
and instead styled the application with simple CSS.

I organized my CSS classes with **BEM** (http://getbem.com), which may not seem important on this case,
but  great on bigger projects. 
Moreover, **CSS variables** were used to easier manage and update colors.

For the movies grid, I used **flexbox** and **media queries** to make it responsive and great-looking in any device.

## FUNCTIONALITY
**App.js** is the place were all the magic happens.
I created a **global state object** to hold the application's state. This way, it was made easier to change
between different views (movies-in-theater / upcoming / search-results) and to prevent unnecessarily fetches 
if we already have the results of a request.

**Api.js** is a file I created to act as a db service for the application. It manages all the api requests
and exports useful methods to be used by the application.

## INFINITE SCROLLING
To achieve the effect of **infinite scrolling**, I created a scroll listener that fires when the user
scrolls the page. However, I made sure to **throttle** this method so it doesn't fire "too often" and
have a negative effect on performance.

This way, when the user is about to view the last movie-cards, a new request is fired to load and
render more movies. (unless there are no more pages to request)
While loading, a temporary **loading indicator** (classic animated dots) is presented at the bottom of the page.

## SEARCH
For searching the entire movie database, I created a text input where the used can type. I made sure to
**debounce** the method so it doesn't fire multiple times on each keystroke. This way, a request to the database
only fires when the user actually stops typing, which improves performance.


## THINGS DONE DIFFERENTLY
- About expanding a card, I did not like the UX of having the entire card clickable to zoom-in and the same to zoom-out.
In mobile devices, it's very easy for the user to accidentaly click on a card and start the sequence. That is why
I decided to add an "expand" icon and a "back" button instead, which are more mobile friendly.

- I noticed not all movies have poster images. To face that, I am checking if there is a poster and if there
is not, I am instead rendering a placeholder image. (which is hosted in an **AWS bucket**)

- I noticed not all movies have trailer/reviews/similar movies. To face that too, If they don't exist in
a particular movie, I am rendering a fallback message to inform the user.

## NEXT STEPS
For the next steps, I would choose to rewrite this application in **React**.
Breaking down the application in smaller **resuable components** will make it much easier to understand it and maintain
it in the future. Furthermore, taking advantage of **React-router**, I would make the back/forward/refresh
buttons of the browser to actually work and navigate the user in the correct place in the application.

## FINAL WORDS
It was great fun working on this assignment. It's nice to work without any frameworks/libraries for a change
and remember the barebones of Web Development. Any feedback will be appreciated, but note that I completed
this assignment in a weekend, bugs are inevitable!

P.S: Thanks for sharing the **API key** with me, your secret will die with me! :)